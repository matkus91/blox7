Untitled
- Delay -
active: false
- Duration - 
lowMin: 1500.0
lowMax: 1500.0
- Count - 
min: 0
max: 40
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 4
scaling0: 0.88235295
scaling1: 1.0
scaling2: 0.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2260274
timeline2: 0.30136988
timeline3: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 1000.0
relative: false
scalingCount: 3
scaling0: 1.0
scaling1: 0.3529412
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.60958904
timeline2: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 5
scaling0: 0.49019608
scaling1: 0.9019608
scaling2: 0.7254902
scaling3: 0.23529412
scaling4: 0.019607844
timelineCount: 5
timeline0: 0.0
timeline1: 0.25342464
timeline2: 0.43150684
timeline3: 0.65068495
timeline4: 0.8150685
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 30.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 90.0
lowMax: 90.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 12
colors0: 1.0
colors1: 1.0
colors2: 1.0
colors3: 1.0
colors4: 0.73333335
colors5: 0.047058824
colors6: 1.0
colors7: 0.12156863
colors8: 0.047058824
colors9: 0.64705884
colors10: 0.078431375
colors11: 0.03137255
timelineCount: 4
timeline0: 0.0
timeline1: 0.24482109
timeline2: 0.50094163
timeline3: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 1.0
scaling1: 1.0
scaling2: 0.75
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.2
timeline2: 0.8
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
C:\Users\Mathias Kuschel\Desktop\particle.png
