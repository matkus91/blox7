package de.mathiaskuschel.blox7.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.mathiaskuschel.blox7.Blox7;

public class DesktopLauncher {
	public static void main(String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height = 450;
		config.width = 800;
		config.useGL30 = true;
		config.resizable = true;
		config.vSyncEnabled = true;
		new LwjglApplication(new Blox7(), config);
	}
}
