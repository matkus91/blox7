package de.mathiaskuschel.blox7.model;

import java.io.IOException;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

public class GameServer {
	public GameServer() {
		Server server = new Server();
		server.start();
		try {
			server.bind(54455, 54677);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Kryo kryo = server.getKryo();
		// kryo.setRegistrationRequired(false);
		kryo.register(Vector2.class);
		kryo.register(SomeRequest.class);
		kryo.register(SomeResponse.class);

		server.addListener(new Listener() {
			public void received(Connection connection, Object object) {
				if (object instanceof SomeRequest) {
					SomeRequest request = (SomeRequest) object;
					System.out.println(request.text);

					SomeResponse response = new SomeResponse();
					response.text = "Thanks";
					connection.sendTCP(response);
				}

				if (object instanceof Vector2) {
					Vector2 destMsg = (Vector2) object;
					System.out.println(destMsg);
				}
			}
		});
	}

	public static class Position {
		public Vector2 pos = new Vector2();
	}

	public static class SomeRequest {
		public String text;
	}

	public static class SomeResponse {
		public String text;
	}
}
