package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.math.Vector2;

public interface HorizonalDirectionListener {
	public void OnVerticalDirectionChanged(Vector2 dir);
}
