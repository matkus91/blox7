package de.mathiaskuschel.blox7.model.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;

import de.mathiaskuschel.blox7.model.shared.ClientInput;
import de.mathiaskuschel.blox7.model.shared.ClientState;
import de.mathiaskuschel.blox7.model.shared.Input;
import de.mathiaskuschel.blox7.model.shared.MovePhysics;
import de.mathiaskuschel.blox7.model.shared.MoveState;
import de.mathiaskuschel.blox7.model.shared.Network;
import de.mathiaskuschel.blox7.model.shared.WorldState;
import tangible.Action1Param;

public class Client {
	/**
	 * Interval with which the physics simulation is updated.
	 */
	public float PhysicsUpdateInterval;

	/**
	 * Indicates if prediction is on/off.
	 */
	public boolean Prediction;

	/**
	 * Indicates if reconciliation is on/off.
	 */
	public boolean Reconciliation;

	/**
	 * Logs the physics simulation steps.
	 */
	private FileHandle log;

	/**
	 * Network interface.
	 */
	private Network network;

	/**
	 * Physics simulation.
	 */
	private final MovePhysics physics = new MovePhysics();

	/**
	 * Sent but unacknowledged inputs.
	 */
	private final Array<ClientInput> unacknowledgedInputs = new Array<ClientInput>();

	/**
	 * Accumulated time which was not used for physics simulation yet (in s).
	 */
	private float accumulatedPhysicsTime;

	/**
	 * Counts the inputs.
	 */
	private int inputCounter;

	/**
	 * Constructor.
	 * 
	 * @param network
	 *            Network interface.
	 */
	public Client(Network network) {
		this.setMoveState(new MoveState());
		this.network = network;
		this.network.StateReceived.addListener("this.OnStateReceived", new Action1Param<WorldState>() {
			@Override
			public void invoke(WorldState t) {
				Client.this.OnStateReceived(t);
			}
		});
		log = Gdx.files.local("client.txt");
	}

	/**
	 * Unique client id.
	 */
	private int ClientId;

	public final int getClientId() {
		return ClientId;
	}

	public final void setClientId(int value) {
		ClientId = value;
	}

	/**
	 * Current move state on client.
	 */
	private MoveState MoveState;

	public final MoveState getMoveState() {
		return MoveState;
	}

	private void setMoveState(MoveState value) {
		MoveState = value;
	}

	/**
	 * Number of unachknowledged inputs.
	 */
	public final int getUnacknowledgedInputsCount() {
		return this.unacknowledgedInputs.size;
	}

	// C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
	/// #endregion

	// C# TO JAVA CONVERTER TODO TASK: There is no preprocessor in Java:
	/// #region Public Methods and Operators

	public final void Update(float deltaTime) {
		this.accumulatedPhysicsTime += deltaTime;

		while (this.accumulatedPhysicsTime >= this.PhysicsUpdateInterval) {
			this.FixedUpdate(this.PhysicsUpdateInterval);

			this.accumulatedPhysicsTime -= this.PhysicsUpdateInterval;
		}
	}

	/**
	 * Fetches the current input on client.
	 * 
	 * @return Current input.
	 */
	private Input FetchInput() {
		Input input;
		if (NativeKeyboard.IsKeyDown(KeyCode.Left)) {
			input = new Input(-1);
		} else if (NativeKeyboard.IsKeyDown(KeyCode.Right)) {
			input = new Input(1);
		} else {
			input = new Input(0);
		}
		return input;
	}

	private void FixedUpdate(float updateInterval) {
		// Get input.
		Input input = this.FetchInput();

		// Send to server.
		ClientInput clientInput = new ClientInput(getClientId(), input, inputCounter++);
		this.network.SendToServer(clientInput);

		if (this.Prediction) {
			this.unacknowledgedInputs.add(clientInput);

			// Apply input to state.
			this.setMoveState(this.physics.TickSimulation(this.getMoveState(), input, updateInterval));

			if (input.dirKey != 0) {
				this.log.writeString("{0} {1}" + (input.dirKey == -1 ? 'L' : 'R') + this.getMoveState().X, true);
			}
		}
	}

	/**
	 * Callback when world state was received via network.
	 * 
	 * @param state
	 *            Received world state.
	 */
	private void OnStateReceived(WorldState state) {
		// Get own state.
		ClientState clientState = state.GetClientState(this.getClientId());
		if (clientState == null) {
			return;
		}

		// Remove inputs which arrived at server.
		while (this.unacknowledgedInputs.size > 0
				&& this.unacknowledgedInputs.peek().inputNumber <= clientState.InputNumber) {
			this.unacknowledgedInputs.removeIndex(unacknowledgedInputs.size - 1);
		}

		MoveState newMoveState = clientState.MoveState;

		if (this.Reconciliation) {
			// Re-apply unacknowledged inputs.
			// C# TO JAVA CONVERTER TODO TASK: There is no equivalent to
			// implicit typing in Java:
			for (int i = 0; i < unacknowledgedInputs.size; i++) {
				newMoveState = this.physics.TickSimulation(newMoveState, unacknowledgedInputs.get(i).input,
						this.PhysicsUpdateInterval);

			}
		}

		// Set new move state.
		this.setMoveState(newMoveState);
	}
}
