package de.mathiaskuschel.blox7.model.client;

// --------------------------------------------------------------------------------------------------------------------

// <copyright file="NativeKeyboard.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

/**
 * Provides keyboard access.
 */
public final class NativeKeyboard {
	/**
	 * A positional bit flag indicating the part of a key state denoting key
	 * pressed.
	 */
	private static final int KeyPressed = 0x8000;

	/**
	 * Returns a value indicating if a given key is pressed.
	 * 
	 * @param key
	 *            The key to check.
	 * @return <c>true</c> if the key is pressed, otherwise <c>false</c>.
	 * 
	 */
	public static boolean IsKeyDown(KeyCode key) {
		return (GetKeyState(key.getValue()) & KeyPressed) != 0;
	}

	/**
	 * Gets the key state of a key.
	 * 
	 * @param key
	 *            Virtuak-key code for key.
	 * @return The state of the key.
	 */
	private static native short GetKeyState(int key);

	static {
		System.loadLibrary("user32.dll");
	}
}