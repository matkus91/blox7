package de.mathiaskuschel.blox7.model.client;

// --------------------------------------------------------------------------------------------------------------------
// <copyright file="KeyCode.cs" company="Slash Games">
//   Copyright (c) Slash Games. All rights reserved.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

/** 
   Codes representing keyboard keys.
 
 
   Key code documentation:
   http://msdn.microsoft.com/en-us/library/dd375731%28v=VS.85%29.aspx
 
*/
public enum KeyCode
{
	/** 
	   The left arrow key.
	*/
	Left(0x25),

	/** 
	   The up arrow key.
	*/
	Up(38),

	/** 
	   The right arrow key.
	*/
	Right(39),

	/** 
	   The down arrow key.
	*/
	Down(40);

	private int intValue;
	private static java.util.HashMap<Integer, KeyCode> mappings;
	private static java.util.HashMap<Integer, KeyCode> getMappings()
	{
		if (mappings == null)
		{
			synchronized (KeyCode.class)
			{
				if (mappings == null)
				{
					mappings = new java.util.HashMap<Integer, KeyCode>();
				}
			}
		}
		return mappings;
	}

	private KeyCode(int value)
	{
		intValue = value;
		getMappings().put(value, this);
	}

	public int getValue()
	{
		return intValue;
	}

	public static KeyCode forValue(int value)
	{
		return getMappings().get(value);
	}
}