package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;

public class Avatar implements PlayerListener {// extends DynamicGameObject {
	private World world;
	public int collided = 0;
	// links = true
	public static final float WORLD_TO_BOX = 1f / 36f;
	public static final float BOX_TO_WORLD = 36;
	Body playerBody;
	private BodyDef playerDef;
	Movement movement = Movement.run;

	static enum Movement {
		run, jump, idle
	}

	public Avatar(World world) {
		this.world = world;
		// super(0, 0, Tile.size, Tile.size);
		Vector2 pos = new Vector2();
		destination.set(pos);
		bounds = new Rectangle(0, 0, Tile.size, Tile.size);

		playerDef = new BodyDef();
		playerDef.type = BodyType.DynamicBody;
		playerDef.fixedRotation = true;
		playerDef.position.set(pos.cpy().scl(WORLD_TO_BOX));
		playerBody = world.physics.createBody(playerDef);
		// setze die Masse auf unendlich, damit sich das Verschieben nicht
		// verzögert
		MassData md = playerBody.getMassData();
		md.mass = 3;// Float.POSITIVE_INFINITY;
		playerBody.setMassData(md);

		PolygonShape playerShape = new PolygonShape();
		playerShape.setAsBox(64 / 2 * WORLD_TO_BOX, 64 / 2 * WORLD_TO_BOX);

		FixtureDef fixtureDef = new FixtureDef();
		fixtureDef.shape = playerShape;
		// Dichte
		fixtureDef.density = 0f;
		// Reibung
		fixtureDef.friction = 0f;
		// zurückprallen
		fixtureDef.restitution = 0f;

		// Fixture fixture =
		playerBody.createFixture(fixtureDef);
	}

	Vector2 dir = new Vector2();
	private double velocity = 200;// 450;
	Vector2 velocityVector = new Vector2();

	void setVelocityVector(double winkel) {
		velocityVector.set((float) (velocity * Math.cos(winkel)), (float) (velocity * Math.sin(winkel)));
	}

	void setVelocityVector(Vector2 dir) {
		// Geschwindigkeit für x und y-Richtung ausrechnen
		// (geht auch über Stahlensatz, aber sqrt() benötigt)
		velocityVector.set((float) (velocity * Math.cos(Math.atan2(dir.y, dir.x))),
				(float) (velocity * Math.sin(Math.atan2(dir.y, dir.x))));
	}

	public Vector2 getDir() {
		return dir;
	}

	public Vector2 getVelocityVector() {
		return velocityVector;
	}

	private boolean reverseAnim = false;
	boolean animDirection = false;

	private void updateAnimationDirection(Vector2 dir) {
		if (dir.x > 0) {
			animDirection = false;
		} else if (dir.x < 0) {
			animDirection = true;
		}
		if (dir.x != 0 && reverseAnim)
			animDirection = !animDirection;
	}

	// nur wenn das Zwischenziel erreicht wurde, kann ein neues gesetzt werden
	public boolean xReached = true;
	public boolean yReached = true;
	// Zwischenziel
	Vector2 oldDestination = new Vector2();
	Vector2 destination = new Vector2();

	public void processDirections() {
		float tileX = getPos().x / Tile.size;
		float tileY = getPos().y / Tile.size;
		// bei ganzzahliger Division dekrementieren/inkrementieren, da
		// math.ceil() / floor() sonst nicht den nächsten, sondern denselben
		// Wert zurückgeben würde
		if (getPos().x % Tile.size == 0) {
			if (dir.x < 0)
				tileX--;
			if (dir.x > 0)
				tileX++;
		}
		if (getPos().y % Tile.size == 0) {
			if (dir.y < 0)
				tileY--;
			if (dir.y > 0)
				tileY++;
		}

		if (dir.y > 0)
			destination.y = (float) Math.ceil(tileY) * Tile.size;
		if (dir.x < 0)
			destination.x = (float) Math.floor(tileX) * Tile.size;
		if (dir.y < 0)
			destination.y = (float) Math.floor(tileY) * Tile.size;
		if (dir.x > 0)
			destination.x = (float) Math.ceil(tileX) * Tile.size;

		if (Gdx.app.getType() != ApplicationType.Desktop && !oldDestination.equals(destination))
			world.gameClient.sendDest(destination);
		oldDestination.set(destination);

		if (dir.x != 0)
			xReached = false;
		if (dir.y != 0)
			yReached = false;

		// dir.set(b2f(right) - b2f(left), b2f(up) - b2f(down));
	}

	public void go(boolean backwards) {
		velocity = 200;
		updateAnimationDirection(dir);
		if (backwards && dir.x != 0) {
			velocity = 50;
			reverseAnim = true;
		} else
			reverseAnim = false;
		setVelocityVector(dir);
	}

	public void destinationReached() {
		// lieber dir als body.linearVelocity, da bei Kollision velocity
		// umgekehrt wird
		if ((dir.x < 0 && getPos().x <= destination.x) || (dir.x > 0 && getPos().x >= destination.x)) {
			collided = 0;
			if (!world.inputManager.isxKeyPressed()) {
				xReached = true;
				playerBody.setLinearVelocity(0, playerBody.getLinearVelocity().y);
				setX(destination.x);
				dir.x = 0;
			} else {
				processDirections();
			}
			if (!yReached)
				go(false);
		}
		if ((dir.y < 0 && getPos().y <= destination.y) || (dir.y > 0 && getPos().y >= destination.y)) {
			collided = 0;
			if (!world.inputManager.isyKeyPressed()) {
				yReached = true;
				playerBody.setLinearVelocity(playerBody.getLinearVelocity().x, 0);
				setY(destination.y);
				dir.y = 0;
			} else
				processDirections();
			if (!xReached)
				go(false);
		}
		// // Wenn der Richtungsvektor und der restliche Weg HALBWEGS
		// // (Skalarprodukt > 0 ) in die selbe Richtung zeigen ->
		// // weiterbewegen
		// if (dir.dot(destination.x - getPos().x, destination.y - getPos().y) >
		// 0) {
		// destinationReached = false;
		// return false;
	}

	public void checkCollided() {
		checkStagnation(oldPos.x, oldPos.y, getPos().x, getPos().y);
		if (dir.x != 0 && (sameX || repeatedCollision) && !world.inputManager.isxKeyPressed()) {
			sameX = false;
			dir.x = -dir.x;
			processDirections();
			go(true);
			// rückwärts nach Kollission zurück laufen, Avatar spiegeln
		}
		if (dir.y != 0 && (sameY || repeatedCollision) && !world.inputManager.isyKeyPressed()) {
			sameY = false;
			dir.y = -dir.y;
			processDirections();
			go(true);
			// rückwärts nach Kollission zurück laufen, Avatar spiegeln
		}
		repeatedCollision = false;

		oldPos.set(getPos());
	}

	public final Rectangle bounds;
	public boolean repeatedCollision;
	public boolean sameX;
	public boolean sameY;
	public Vector2 roundedDirection = new Vector2();
	public boolean vertical;
	private Vector2 oldPos = new Vector2();
	private Vector2 helpVec = new Vector2();

	public Vector2 getPos() {
		helpVec.set(playerBody.getPosition());
		return helpVec.scl(BOX_TO_WORLD);
		// return playerBody.getPosition().scl(BOX_TO_WORLD);
	}

	public void setPos(Vector2 newPos) {
		setPos(newPos.x, newPos.y);
	}

	public void setPos(float x, float y) {
		checkStagnation(oldPos.x, oldPos.y, x, y);
		Vector2 pos = playerBody.getPosition();
		roundDirection(pos.x, pos.y, x, y);
		oldPos.set(pos).scl(BOX_TO_WORLD);
		playerBody.setTransform(x * WORLD_TO_BOX, y * WORLD_TO_BOX, 0);
	}

	public void setX(float x) {
		monitorX(oldPos.x, x);
		Vector2 pos = playerBody.getPosition();
		oldPos.x = pos.x * BOX_TO_WORLD;
		playerBody.setTransform(x * WORLD_TO_BOX, pos.y, 0);
	}

	public void setY(float y) {
		monitorY(oldPos.y, y);
		Vector2 pos = playerBody.getPosition();
		oldPos.y = pos.y * BOX_TO_WORLD;
		playerBody.setTransform(pos.x, y * WORLD_TO_BOX, 0);
	}

	Vector2 getOldPos() {
		return oldPos;
	}

	private void checkStagnation(float x1, float y1, float x2, float y2) {
		sameX = Math.abs(x2 - x1) < 0.2;
		sameY = Math.abs(y2 - y1) < 0.2;
	}

	private void monitorX(float x1, float x2) {
		sameX = Math.abs(x2 - x1) < 0.3;
	}

	private void monitorY(float y1, float y2) {
		sameY = Math.abs(y2 - y1) < 0.3;
	}

	private void roundDirection(float x1, float y1, float x2, float y2) {
		double winkel = Math.atan2(y2 - y1, x2 - x1);
		roundedDirection.set(Math.round(Math.cos(winkel)), Math.round((float) Math.sin(winkel)));
	}
}
