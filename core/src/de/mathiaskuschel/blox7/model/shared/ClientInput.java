package de.mathiaskuschel.blox7.model.shared;

public class ClientInput {
	/// <summary>
	/// Id of client who send the input.
	/// </summary>
	public int clientId;

	/// <summary>
	/// Input data.
	/// </summary>
	public Input input;

	/// <summary>
	/// Unique, consecutive input number.
	/// </summary>
	public int inputNumber;

	public ClientInput(int clientId, Input input, int inputNumer) {
		this.clientId = clientId;
		this.input = input;
		this.inputNumber = inputNumer;
	}
}
