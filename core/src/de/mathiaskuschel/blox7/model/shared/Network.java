package de.mathiaskuschel.blox7.model.shared;

import java.util.ArrayList;

import tangible.Action0Param;
import tangible.Action1Param;
import tangible.Event;

public class Network {
	/**
	 * Delayed actions to perform after a lag.
	 */
	private final ArrayList<DelayedAction> actions = new ArrayList<DelayedAction>();

	/**
	 * Lag (in s).
	 */
	private float lag;

	public Network(float lag) {
		this.lag = lag;
	}

	/**
	 * Called when client input was received.
	 */
	public tangible.Event<tangible.Action1Param<ClientInput>> InputReceived = new tangible.Event<tangible.Action1Param<ClientInput>>();

	/**
	 * Called when a world state was received.
	 */
	public tangible.Event<tangible.Action1Param<WorldState>> StateReceived = new tangible.Event<tangible.Action1Param<WorldState>>();

	/**
	 * Sends the specified world state to all clients.
	 * 
	 * @param state
	 *            State to send.
	 */
	public void SendToClients(final WorldState state) {
		this.AddLagAction(new Action0Param() {
			@Override
			public void invoke() {
				Network.this.OnStateReceived(state);
			}
		}, this.lag);
	}

	/**
	 * Sends the specified client input to the server.
	 * 
	 * @param clientInput
	 *            Client input to send.
	 */
	public void SendToServer(final ClientInput clientInput) {
		this.AddLagAction(new Action0Param() {
			@Override
			public void invoke() {
				Network.this.OnInputReceived(clientInput);
			}
		}, this.lag);
	}

	public void Update(float deltaTime) {
		// Check delayed actions.
		for (int index = this.actions.size() - 1; index >= 0; --index) {
			DelayedAction delayedAction = this.actions.get(index);
			delayedAction.RemainingDelay -= deltaTime;
			if (delayedAction.RemainingDelay <= 0.0f) {
				delayedAction.Action.invoke();
				this.actions.remove(index);
			}
		}
	}

	private void AddLagAction(tangible.Action0Param action, float actionLag) {
		DelayedAction tempVar = new DelayedAction();
		tempVar.RemainingDelay = actionLag;
		tempVar.Action = action;
		this.actions.add(tempVar);
	}

	private void OnInputReceived(ClientInput obj) {
		Event<Action1Param<ClientInput>> handler = this.InputReceived;
		if (handler != null) {
			for (int i = 0; i < handler.listeners().size(); i++) {
				if (handler.listeners().get(i) != null)
					handler.listeners().get(i).invoke(obj);
			}
		}
	}

	private void OnStateReceived(WorldState obj) {
		Event<tangible.Action1Param<WorldState>> handler = this.StateReceived;
		if (handler != null) {
			for (int i = 0; i < handler.listeners().size(); i++) {
				if (handler.listeners().get(i) != null)
					handler.listeners().get(i).invoke(obj);
			}
		}
	}

	private static class DelayedAction {

		/**
		 * Action to perform after delay.
		 */
		public tangible.Action0Param Action;

		/**
		 * Remaining delay of action (in s).
		 */
		public float RemainingDelay;
	}
}
