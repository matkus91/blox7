package de.mathiaskuschel.blox7.model.shared;

public class MovePhysics {
	/// <summary>
	/// Fixed speed if moved.
	/// </summary>
	public final float Speed = 10.0f;

	/// <summary>
	/// Performs one simulation step for the specified move state, input and
	/// time delta.
	/// </summary>
	/// <param name="moveState">Current move state.</param>
	/// <param name="input">Current input.</param>
	/// <param name="deltaTime">Delta time to simulate.</param>
	/// <returns>New move state after simulation step.</returns>
	public MoveState TickSimulation(MoveState moveState, Input input, float deltaTime) {
		MoveState newState = new MoveState();
		if (input != null) {
			newState.X = moveState.X + deltaTime * input.dirKey * Speed;
		}
		return newState;
	}
}
