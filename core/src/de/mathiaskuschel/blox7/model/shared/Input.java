package de.mathiaskuschel.blox7.model.shared;

public class Input {
	public final int dirKey;

	public Input(int dirKey) {
		this.dirKey = dirKey;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (obj == this) {
			return true;
		}
		if (obj.getClass() != this.getClass()) {
			return false;
		}
		return this.Equals((Input) obj);
	}

	@Override
	public int hashCode() {
		return this.dirKey;
	}

	protected boolean Equals(Input other) {
		return this.dirKey == other.dirKey;
	}
}
