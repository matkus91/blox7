package de.mathiaskuschel.blox7.model.shared;

import java.util.HashMap;

public class WorldState {
	/// <summary>
	/// Maps character id to the current move state of the character.
	/// </summary>

	public HashMap<Integer, ClientState> clientStates;

	/// <summary>
	/// Returns the state for the client with the specified id.
	/// </summary>
	/// <param name="clientId">Id of client to get state for.</param>
	/// <returns>State of client with specified id.</returns>
	public ClientState GetClientState(int clientId) {
		ClientState clientState = clientStates.get(clientId);
		return clientState;
	}

}
