package de.mathiaskuschel.blox7.model.shared;

public class MoveState {
	// Position.
	public float X;
	public float Y;

	/**
	 * Linear interpolation between two move states.
	 * 
	 * @param moveStateA
	 *            First move state.
	 * @param moveStateB
	 *            Second move state.
	 * @param t
	 *            Factor of linear interpolation.
	 * @return Linear interpolated move state between the two specified move
	 *         states.
	 */
	public final MoveState Lerp(MoveState moveStateA, MoveState moveStateB, float t) {
		MoveState tempVar = new MoveState();
		tempVar.X = moveStateA.X * (1 - t) + moveStateB.X * t;
		tempVar.Y = moveStateA.Y * (1 - t) + moveStateB.Y * t;
		return tempVar;
	}

	@Override
	public String toString() {
		return String.format("X: %1$-6s", Math.round(this.X * Math.pow(10, 2)) / Math.pow(10, 2));
	}

	public final String Visualize(int range) {
		int position = (int) this.X + range / 2;
		if (position < 0) {
			position = 0;
		}
		if (position > range) {
			position = range;
		}
		return new String(" " + position + "O " + (range - position));
	}
}
