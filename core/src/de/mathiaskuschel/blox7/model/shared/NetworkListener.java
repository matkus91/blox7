package de.mathiaskuschel.blox7.model.shared;

public interface NetworkListener {
	/// Called when client input was received.
	public void InputReceived(ClientInput clientInput);

	/// Called when a world state was received.
	public void StateReceived(WorldState worldState);

	void OnInputReceived(ClientInput obj);

	void OnStateReceived(WorldState obj);
}
