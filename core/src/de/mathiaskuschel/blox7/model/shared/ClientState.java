package de.mathiaskuschel.blox7.model.shared;

public class ClientState {
    /// <summary>
    ///   Latest input which was applied to the move state.
    /// </summary>
    public int InputNumber;

    /// <summary>
    ///   Current move state on server.
    /// </summary>
    public MoveState MoveState;
}
