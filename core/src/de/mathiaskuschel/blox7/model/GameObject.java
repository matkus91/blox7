/*******************************************************************************
 * Copyright 2011 See AUTHORS file.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class GameObject {
	private Body body;
	private Vector2 pos = new Vector2();
	public static final float WORLD_TO_BOX = World.WORLD_TO_BOX;
	public static final float BOX_TO_WORLD = World.BOX_TO_WORLD;

	public GameObject(float x, float y, Body body) {
		this.body = body;
		pos.set(x, y);
	}

	public void setPos(Vector2 position) {
		body.setTransform(position, 0);
	}

	public Vector2 getPos() {
		return body.getPosition().scl(BOX_TO_WORLD);
	}

}
