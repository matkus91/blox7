package de.mathiaskuschel.blox7.model;

import java.util.Random;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Shape;

public class Box {
	public static int size = 72;
	Color color;
	// float stateTime = 0;
	static Random rand = new Random();
	public Sprite leftUp;
	public Sprite rightUp;
	public Sprite leftDown;
	public Sprite rightDown;
	public Body body;
	public Vector2 destination = new Vector2();
	public Vector2 dir = new Vector2();
	public boolean vertical;
	public Shape shape;
	public boolean isVisible = false;
	// Es gibt drei Möglichkeiten die Box zu stoppe:
	// 1. destination Reached (Ziel erreicht)
	// 2. box kollidiert mit anderer box
	// 3. wenn box geschwindigkeit 0 ist
	public boolean stop = false;
	public boolean push = false;
	public boolean firstCheck = false;
	public Contact contact;
	// beim Bewegen ist tile null
	public Tile tile;

	public Box(Tile tile) {
		this.tile = tile;
	}

	private Vector2 pos = new Vector2();
	protected boolean hadContact = false;
	public boolean setBody = false;

	public Vector2 getPos() {
		pos.set(body.getPosition()).scl(World.BOX_TO_WORLD);
		return pos;
	}
}
