package de.mathiaskuschel.blox7.model;

import java.io.IOException;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Client;;

public class GameClient {
	public Client client;

	public GameClient() {
		client = new Client();
		client.start();
		try {
			client.connect(5000, "192.168.178.23", 54455, 54677);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Kryo kryo = client.getKryo();
		// kryo.setRegistrationRequired(false);
		kryo.register(Vector2.class);
		kryo.register(SomeRequest.class);
		kryo.register(SomeResponse.class);
		kryo.register(Input.class);

		client.addListener(new Listener() {
			public void received(Connection connection, Object object) {
				if (object instanceof SomeResponse) {
					SomeResponse response = (SomeResponse) object;
					System.out.println(response.text);
				}
			}
		});
		SomeRequest request = new SomeRequest();
		request.text = "Here is the request";
		client.sendTCP(request);
	}

	public void sendDest(Vector2 dest) {
		client.sendTCP(dest);
	}

	public static class Position {
		public Vector2 pos = new Vector2();
	}

	public static class SomeRequest {
		public String text;
	}

	public static class SomeResponse {
		public String text;
	}
}
