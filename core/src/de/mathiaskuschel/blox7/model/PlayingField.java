package de.mathiaskuschel.blox7.model;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;

public class PlayingField {
	private static Random rand = new Random();
	private Tile[][] tiles;
	private float[] tilesX;
	private float[] tilesY;
	private int anzTileWidth;
	private int anzTileHeight;
	private int size;
	private int upperRow;
	private int lowerRow = 0;
	private int leftCol = 0;
	private int rightCol;
	public static final float WORLD_TO_BOX = World.WORLD_TO_BOX;
	public static final float BOX_TO_WORLD = World.BOX_TO_WORLD;
	// private TextureRegion region = new TextureRegion(new Texture(
	// Gdx.files.internal("img/tile.png")));
	private Texture texture = new Texture(Gdx.files.internal("img/tile.png"));

	private Sprite vertexLeftUp;
	private Sprite vertexRightUp;
	private Sprite vertexLeftDown;
	private Sprite vertexRightDown;
	private Sprite body;
	private Sprite innerVertexLeftUp;
	private Sprite innerVertexRightUp;
	private Sprite innerVertexLeftDown;
	private Sprite innerVertexRightDown;
	private Sprite sideUp;
	private Sprite sideLeft;
	private Sprite sideRight;
	private Sprite sideDown;
	private World world;

	public PlayingField(World world, int anzTileWidth, int anzTileHeigth) {
		this.world = world;
		// anzTileWidth += 2;
		// anzTileHeigth += 2;
		rightCol = anzTileWidth;
		upperRow = anzTileHeigth;
		// rightCol ist eins kleiner als anzTileWidth, da rightCol ein Index
		// ist, und anzTileWidth die breite des Arrays
		anzTileWidth += 1;
		anzTileHeigth += 1;
		this.anzTileWidth = anzTileWidth;
		this.anzTileHeight = anzTileHeigth;
		size = Tile.size;
		tilesX = new float[anzTileWidth];
		tilesY = new float[anzTileHeigth];
		tiles = new Tile[anzTileWidth][anzTileHeigth];
		for (int i = 0; i < anzTileWidth; i++) {
			for (int j = 0; j < anzTileHeigth; j++) {
				tiles[i][j] = new Tile();
				// Startfeld soll leer sein
				if (i != 0 && j != 0) {
					int nr = rand.nextInt(7);
					if (nr >= 4) {
						tiles[i][j].box1 = new Box(tiles[i][j]);
						if (nr == 4)
							tiles[i][j].box1.color = Color.YELLOW;
						else if (nr == 5)
							tiles[i][j].box1.color = Color.RED;
						else if (nr == 6)
							tiles[i][j].box1.color = Color.GREEN;
					}
				}
			}
		}
		for (int i = 0; i < anzTileWidth; i++) {
			tilesX[i] = i * size;
		}
		for (int i = 0; i < anzTileHeigth; i++) {
			tilesY[i] = i * size;
		}
		renderer.setAutoShapeType(true);
		// Origin links oben
		TextureRegion vertexLeftUpRegion = new TextureRegion(texture, 0, 0, 36, 36);
		TextureRegion bodyRegion = new TextureRegion(texture, 18, 18, 36, 36);
		TextureRegion innerVertexLeftUpRegion = new TextureRegion(texture, 0, 36, 36, 36);
		TextureRegion regionSideUp = new TextureRegion(texture, 36, 0, 36, 36);
		TextureRegion regionSideRight = new TextureRegion(texture, 36, 39, 36, 36);

		vertexLeftUp = new Sprite(vertexLeftUpRegion);
		vertexRightUp = new Sprite(vertexLeftUpRegion);
		vertexLeftDown = new Sprite(vertexLeftUpRegion);
		vertexRightDown = new Sprite(vertexLeftUpRegion);

		vertexRightUp.flip(true, false);
		vertexLeftDown.flip(false, true);
		vertexRightDown.flip(true, true);

		innerVertexLeftUp = new Sprite(innerVertexLeftUpRegion);
		innerVertexRightUp = new Sprite(innerVertexLeftUpRegion);
		innerVertexLeftDown = new Sprite(innerVertexLeftUpRegion);
		innerVertexRightDown = new Sprite(innerVertexLeftUpRegion);

		innerVertexRightUp.flip(true, false);
		innerVertexLeftDown.flip(false, true);
		innerVertexRightDown.flip(true, true);

		body = new Sprite(bodyRegion);

		sideUp = new Sprite(regionSideUp);
		sideLeft = new Sprite(regionSideRight);
		sideRight = new Sprite(regionSideRight);
		sideDown = new Sprite(regionSideUp);

		// sideLeft.rotate(270);
		// sideRight.rotate(90);
		// sideDown.rotate(180);

		// sideLeft.setOriginCenter();
		// sideLeft.setRotation(270);
		sideLeft.flip(true, false);
		sideDown.flip(false, true);

		// sideLeft.rotate90(false);
		// sideRight.rotate90(true);
		// sideDown.rotate90(true);
		// sideDown.rotate90(true);

		for (int i = 0; i < anzTileWidth; i++) {
			for (int j = 0; j < anzTileHeigth; j++) {
				Box box = tiles[i][j].box1;
				if (box != null) {
					boolean left = false;
					boolean right = false;
					boolean up = false;
					boolean down = false;
					boolean leftUp = false;
					boolean rightUp = false;
					boolean leftDown = false;
					boolean rightDown = false;
					if (box.equals(tiles[i][(j + 1) % anzTileHeigth].box1 != null))
						if (box.equals(tiles[i][(j + 1) % anzTileHeigth].box1.color))
							up = true;
					if (box.equals(tiles[(i + 1) % anzTileWidth][j].box1 != null))
						if (box.equals(tiles[(i + 1) % anzTileWidth][j].box1.color))
							right = true;
					if (tiles[(i - 1 + anzTileWidth) % anzTileWidth][j].box1 != null)
						if (box.equals(tiles[(i - 1 + anzTileWidth) % anzTileWidth][j].box1.color))
							left = true;
					if (tiles[i][(j - 1 + anzTileHeigth) % anzTileHeigth].box1 != null)
						if (box.equals(tiles[i][(j - 1 + anzTileHeigth) % anzTileHeigth].box1.color))
							down = true;
					if (tiles[(i - 1 + anzTileWidth) % anzTileWidth][(j + 1) % anzTileHeigth].box1 != null)
						if (box.equals(
								tiles[(i - 1 + anzTileWidth) % anzTileWidth][(j + 1) % anzTileHeigth].box1.color))
							leftUp = true;
					if (tiles[(i + 1) % anzTileWidth][(j + 1) % anzTileHeigth].box1 != null)
						if (box.equals(tiles[(i + 1) % anzTileWidth][(j + 1) % anzTileHeigth].box1.color))
							rightUp = true;
					if (tiles[(i - 1 + anzTileWidth) % anzTileWidth][((j - 1) + anzTileHeigth)
							% anzTileHeigth].box1 != null)
						if (box.equals(tiles[(i - 1 + anzTileWidth) % anzTileWidth][(j - 1 + anzTileHeigth)
								% anzTileHeigth].box1.color))
							leftDown = true;
					if (tiles[(i + 1) % anzTileWidth][(j - 1 + anzTileHeigth) % anzTileHeigth].box1 != null)
						if (box.equals(
								tiles[(i + 1) % anzTileWidth][(j - 1 + anzTileHeigth) % anzTileHeigth].box1.color))
							rightDown = true;

					if (left && up && leftUp)
						box.leftUp = body;
					else if (left && up && !leftUp)
						box.leftUp = innerVertexLeftUp;
					else if (left && !up)
						box.leftUp = sideUp;
					else if (!left && up)
						box.leftUp = sideLeft;
					else if (!left && !up)
						box.leftUp = vertexLeftUp;

					if (right && up && rightUp)
						box.rightUp = body;
					else if (right && up && !rightUp)
						box.rightUp = innerVertexRightUp;
					else if (right && !up)
						box.rightUp = sideUp;
					else if (!right && up)
						box.rightUp = sideRight;
					else if (!right && !up)
						box.rightUp = vertexRightUp;

					if (left && down && leftDown)
						box.leftDown = body;
					else if (left && down && !leftDown)
						box.leftDown = innerVertexLeftDown;
					else if (left && !down)
						box.leftDown = sideDown;
					else if (!left && down)
						box.leftDown = sideLeft;
					else if (!left && !down)
						box.leftDown = vertexLeftDown;

					if (right && down && rightDown)
						box.rightDown = body;
					else if (right && down && !rightDown)
						box.rightDown = innerVertexRightDown;
					else if (right && !down)
						box.rightDown = sideDown;
					else if (!right && down)
						box.rightDown = sideRight;
					else if (!right && !down)
						box.rightDown = vertexRightDown;
				}
			}
		}
	}

	public void shiftUp(int width) {
		for (int j = 0; j < width; j++) {
			tilesY[lowerRow] = tilesY[upperRow] + size;
			upperRow = (upperRow + 1) % anzTileHeight;
			lowerRow = (lowerRow + 1) % anzTileHeight;
		}
	}

	public void shiftDown(int width) {
		for (int j = 0; j < width; j++) {
			tilesY[upperRow] = tilesY[lowerRow] - size;
			upperRow = (upperRow + anzTileHeight - 1) % anzTileHeight;
			lowerRow = (lowerRow + anzTileHeight - 1) % anzTileHeight;
		}
	}

	public void shiftRight(int width) {
		for (int j = 0; j < width; j++) {
			tilesX[leftCol] = tilesX[rightCol] + size;
			rightCol = (rightCol + 1) % anzTileWidth;
			leftCol = (leftCol + 1) % anzTileWidth;
		}
	}

	public void shiftLeft(int width) {
		for (int j = 0; j < width; j++) {
			tilesX[rightCol] = tilesX[leftCol] - size;
			rightCol = (rightCol + anzTileWidth - 1) % anzTileWidth;
			leftCol = (leftCol + anzTileWidth - 1) % anzTileWidth;
		}
	}

	// Ränder aktualisieren
	public boolean update(Camera camera) {
		// überprüfe ob nachste Kachel am Rand geladen werden muss
		if (camera.position.x - camera.viewportWidth / 2 <= tilesX[leftCol] - size / 2) {
			shiftLeft(1);
			update(camera);
			return true;
		} else if (camera.position.x + camera.viewportWidth / 2 >= tilesX[rightCol] + size / 2) {
			shiftRight(1);
			update(camera);
			return true;
		}
		if (camera.position.y - camera.viewportHeight / 2 <= tilesY[lowerRow] - size / 2) {
			shiftDown(1);
			update(camera);
			return true;
		} else if (camera.position.y + camera.viewportHeight / 2 >= tilesY[upperRow] + size / 2) {
			shiftUp(1);
			update(camera);
			return true;
		}
		return false;
	}

	public Tile getTile(Vector2 pos) {
		return getTile(pos.x, pos.y);
	}

	public Tile getTile(float x, float y) {
		return tiles[getCol(x)][getRow(y)];
	}

	// public int getCol(float x) {
	// double help = (x - tilesX[leftCol]) / Tile.size;
	// int col = (int) Math.floor(help);
	// col = (col + leftCol) % anzTileWidth;
	// if (col < 0)
	// col += anzTileWidth;
	// return col;
	// }
	public int getCol(float x) {
		double help = (x - tilesX[leftCol]) / Tile.size;
		int col = (int) Math.round(help);
		col = (col + leftCol) % anzTileWidth;
		if (col < 0)
			col += anzTileWidth;
		return col;
	}

	// public int getRow(float y) {
	// double help = (y - tilesY[lowerRow]) / Tile.size;
	// int row = (int) Math.floor(help);
	// row = (row + lowerRow) % anzTileHeight;
	// if (row < 0)
	// row += anzTileHeight;
	// return row;
	// }
	public int getRow(float y) {
		double help = (y - tilesY[lowerRow]) / Tile.size;
		int row = (int) Math.round(help);
		row = (row + lowerRow) % anzTileHeight;
		if (row < 0)
			row += anzTileHeight;
		return row;
	}

	private ShapeRenderer renderer = new ShapeRenderer();
	// true um sofort zu rendern
	private Vector2 renderPos = new Vector2();

	public void render(OrthographicCamera camera, SpriteBatch batch) {
		// renderer.begin();
		// renderer.setProjectionMatrix(camera.combined);
		// renderer.set(ShapeType.Filled);

		resetVisible();
		// Begrenzungen von sichtbaren Kacheln
		int left = getCol(camera.position.x - camera.viewportWidth / 2);
		int right = getCol(camera.position.x + camera.viewportWidth / 2);
		int down = getRow(camera.position.y - camera.viewportHeight / 2);
		int up = getRow(camera.position.y + camera.viewportHeight / 2);

		// sichtbare Kacheln zeichen
		int i = left;
		while (true) {
			int j = down;
			while (true) {
				Box box = tiles[i][j].box1;
				if (box != null) {
					batch.setColor(box.color);
					batch.draw(box.leftUp, tilesX[i] - 36, tilesY[j]);
					batch.draw(box.rightUp, tilesX[i], tilesY[j]);
					batch.draw(box.leftDown, tilesX[i] - 36, tilesY[j] - 36);
					batch.draw(box.rightDown, tilesX[i], tilesY[j] - 36);

					// renderer.setColor(box);
					// renderer.rect(tilesX[i], tilesY[j], size, size);

					box.isVisible = true;
					// if (box.box) {
					// shapeRenderer.setProjectionMatrix(camera.combined);
					// shapeRenderer.begin(ShapeType.Filled);
					// shapeRenderer.circle(tilesX[i] + Tile.size / 2,
					// tilesY[j]
					// +
					// Tile.size / 2, 2);
					// // shapeRenderer.setColor(Color.BLACK);
					// shapeRenderer.end();

				}
				if (j == up)
					break;
				j = (j + 1) % anzTileHeight;
			}
			if (i == right)
				break;
			i = (i + 1) % anzTileWidth;
		}
		batch.setColor(1, 1, 1, 1);

		checkBoxes(left, right, down, up, camera, batch);
		// renderer.end();
	}

	private boolean firstTime = true;
	private int oldLeft;
	private int oldRight;
	private int oldDown;
	private int oldUp;
	Vector2 oldCamPos = new Vector2();
	ShapeRenderer shapeRenderer = new ShapeRenderer();

	// setze / lösche bodys wenn nötig
	private void checkBoxes(int left, int right, int down, int up, OrthographicCamera camera, SpriteBatch batch) {
		if (firstTime) {
			oldLeft = left;
			oldRight = right;
			oldDown = down;
			oldUp = up;
			firstTime = false;
		}
		int helpLeft, helpRight, helpDown, helpUp;

		if (camera.position.x < oldCamPos.x) {
			helpLeft = left;
			helpRight = oldRight;
		} else {
			helpLeft = oldLeft;
			helpRight = right;
		}
		if (camera.position.y < oldCamPos.y) {
			helpDown = down;
			helpUp = oldUp;
		} else {
			helpDown = oldDown;
			helpUp = up;
		}
		int i = helpLeft;
		while (true) {
			int j = helpDown;
			while (true) {
				// setzte bodys
				Box box = tiles[i][j].box1;
				if (box != null)
					if (box.body == null && box.isVisible)
						createBodys(i, j);
					// lösche bodys
					else if (box.body != null && !box.isVisible && box.body.getType() != BodyType.DynamicBody) {
						world.physics.destroyBody(box.body);
						box.body = null;
					}

				if (j == helpUp)
					break;
				j = (j + 1) % anzTileHeight;
			}
			if (i == helpRight)
				break;
			i = (i + 1) % anzTileWidth;
		}

		oldLeft = left;
		oldRight = right;
		oldDown = down;
		oldUp = up;
		oldCamPos.set(camera.position.x, camera.position.y);
		// System.out.println(world.physics.getFixtureCount());
	}

	public void processColissions() {
		boxDestinationReached();
		checkStopped();
		checkCollided();
	}

	// die aktuell geschobene Box
	public Box pushedBox;
	private Box lastPushedBox;

	private void checkCollided() {
		// zuletzt bewegte Box bevorzugt schieben
		if (world.collidedBoxes.size != 0 && pushedBox == null) {
			Box box = tryBoxes(lastPushedBox);
			if (box != null && !movingBoxes.contains(box, true)) {
				lastPushedBox = box;
				box.body.setType(BodyType.DynamicBody);
				box.firstCheck = true;
				box.contact.setEnabled(true);
				System.out.println("move box");
				pushedBox = box;
				movingBoxes.add(box);
				Tile collidedTile = getTile(box.body.getPosition().x * BOX_TO_WORLD,
						box.body.getPosition().y * BOX_TO_WORLD);
				collidedTile.box1 = null;

				setDestination(box);

				if (box.body.getFixtureList().size != 0) {
					while (box.body.getFixtureList().size != 0) {
						box.body.destroyFixture(box.body.getFixtureList().get(0));
					}
					PolygonShape helpShape = new PolygonShape();
					// um zu probieren einen Block bei schrägem Laufen an
					// einem
					// anderen vorbeizuschieben:
					helpShape.setAsBox((Tile.size / 2f - .3f) * WORLD_TO_BOX, (Tile.size / 2f - .3f) * WORLD_TO_BOX);
					// if (box.vertical)
					// helpShape.setAsBox((Tile.size / 2f - 5) *
					// WORLD_TO_BOX,
					// Tile.size / 2 * WORLD_TO_BOX);
					// else
					// helpShape.setAsBox(Tile.size / 2 * WORLD_TO_BOX,
					// (Tile.size / 2f - 5) * WORLD_TO_BOX);
					FixtureDef square = new FixtureDef();
					square.shape = helpShape;
					square.density = 0f;
					square.friction = 0f;
					square.restitution = 0f;
					box.body.createFixture(square);
					helpShape.dispose();
				}
			}
		}
	}

	private Array<Box> xPos = new Array<Box>(2);
	private Array<Box> xNeg = new Array<Box>(2);
	private Array<Box> yPos = new Array<Box>(2);
	private Array<Box> yNeg = new Array<Box>(2);

	// probiert bei Mehrfachkollision durch, welche Boxen man verschieben kann
	private Box tryBoxes(Box preferedBox) {
		Box possibleBox = null;
		for (int i = 0; i < world.collidedBoxes.size; i++) {
			Box box = world.collidedBoxes.get(i);
			if (box.dir.x > 0)
				xPos.add(box);
			if (box.dir.x < 0)
				xNeg.add(box);
			if (box.dir.y > 0)
				yPos.add(box);
			if (box.dir.y < 0)
				yNeg.add(box);
		}
		if (preferedBox != null && checkPushDirection(preferedBox))
			if ((xPos.size == 1 && xPos.get(0) == preferedBox) || (xNeg.size == 1 && xNeg.get(0) == preferedBox)
					|| (yPos.size == 1 && yPos.get(0) == preferedBox)
					|| (yNeg.size == 1 && yNeg.get(0) == preferedBox)) {
				clearCollisionLists();
				return preferedBox;
			}

		if (xPos.size == 1 && checkPushDirection(xPos.get(0)))
			possibleBox = xPos.get(0);
		if (xNeg.size == 1 && checkPushDirection(xNeg.get(0)))
			possibleBox = xNeg.get(0);
		if (yPos.size == 1 && checkPushDirection(yPos.get(0)))
			possibleBox = yPos.get(0);
		if (yNeg.size == 1 && checkPushDirection(yNeg.get(0)))
			possibleBox = yNeg.get(0);
		clearCollisionLists();
		return possibleBox;
	}

	private void clearCollisionLists() {
		xPos.clear();
		xNeg.clear();
		yPos.clear();
		yNeg.clear();
	}

	private void setDestination(Box box) {
		float tileX = box.body.getPosition().x * BOX_TO_WORLD / Tile.size;
		float tileY = box.body.getPosition().y * BOX_TO_WORLD / Tile.size;
		// bei ganzzahliger Division dekrementieren/inkrementieren, da
		// math.ceil() / floor() bei ganzen Zahlen nicht den nächsten, sondern
		// denselben
		// Wert zurückgeben würde
		if (tileX == (int) tileX) {
			if (box.dir.x < 0)
				tileX--;
			if (box.dir.x > 0)
				tileX++;
		}
		if (tileY == (int) tileY) {
			if (box.dir.y < 0)
				tileY--;
			if (box.dir.y > 0)
				tileY++;
		}
		if (box.dir.y > 0)
			box.destination.y = (float) Math.ceil(tileY) * Tile.size;
		if (box.dir.x < 0)
			box.destination.x = (float) Math.floor(tileX) * Tile.size;
		if (box.dir.y < 0)
			box.destination.y = (float) Math.floor(tileY) * Tile.size;
		if (box.dir.x > 0)
			box.destination.x = (float) Math.ceil(tileX) * Tile.size;
	}

	private Vector2 helpVec = new Vector2();
	// private Array<Contact> contacts = new Array<Contact>();

	private void checkStopped() {
		boolean anythingStopped = false;
		// System.out.println(world.physics.getContactCount());
		// contacts = world.physics.getContactList();
		for (int i = 0; i < movingBoxes.size; i++) {
			Box box = movingBoxes.get(i);
			// wenn die Zielkachel gesetzt wurde
			if ((box.stop & !box.push)) {// || (!box.firstCheck && box.samePos
											// &&
											// box.body.getLinearVelocity().isZero())))
											// {
				System.out.println("stopped");
				anythingStopped = true;
				box.body.setType(BodyType.StaticBody);
				helpVec.set(box.body.getPosition());
				helpVec.scl(BOX_TO_WORLD);
				// System.out.println(helpVec);
				getSnippingPoint(helpVec, helpVec);
				// System.out.println(helpVec);
				box.tile = getTile(helpVec);
				box.tile.box1 = box;
				helpVec.scl(WORLD_TO_BOX);
				box.body.setTransform(helpVec, 0);
				box.body.setLinearVelocity(0, 0);
				if (box.body.getFixtureList().size != 0) {
					// box.getFixtureList().get(0).getShape().setRadius(36 *
					// WORLD_TO_BOX);
					while (box.body.getFixtureList().size != 0) {
						box.body.destroyFixture(box.body.getFixtureList().get(0));
					}
					createFixtures(box.body);
				}
				if (world.collidedBoxes.contains(box, true))
					world.collidedBoxes.removeValue(box, true);
				movingBoxes.removeIndex(i);
				i--;
				box.stop = false;
			} else if (box.body.getLinearVelocity().isZero() && !box.firstCheck) {
				// box.contact.setEnabled(true);
				box.stop = true;
				box.push = false;
				if (box == pushedBox)
					pushedBox = null;
			}
			if (box.firstCheck)
				box.firstCheck = false;
		}
		if (anythingStopped)
			world.update = true;
	}

	Vector2 helpDir = new Vector2();

	private boolean checkPushDirection(Box box) {
		// keine Ahnung warum, aber manchmal ist body null
		if (box.body == null)
			return false;
		Vector2 pos = box.getPos();
		helpDir.set(box.dir).scl(Tile.size);
		pos.add(helpDir);
		if (getTile(pos).box1 == null)
			return true;
		else
			return false;
	}

	// Gibt immer die Koordinate der nächste Ecke zurück
	public void getSnippingPoint(Vector2 pos, Vector2 back) {
		back.x = Math.round(pos.x / Tile.size) * Tile.size;
		back.y = Math.round(pos.y / Tile.size) * Tile.size;
	}

	// Gibt immer die Koordinate des nächsten Kachelmittelpunkts zurück
	public void getSnippingPoint2(Vector2 pos, Vector2 back) {
		back.x = (float) (Math.floor(pos.x / Tile.size) * Tile.size + Tile.size / 2);
		back.y = (float) (Math.floor(pos.y / Tile.size) * Tile.size + Tile.size / 2);
	}

	public Array<Box> movingBoxes = new Array<Box>();
	private Vector2 tmpPos = new Vector2();

	public void renderDynamics(SpriteBatch batch) {
		for (int i = 0; i < movingBoxes.size; i++) {
			if (movingBoxes.get(i).body != null) {
				Box box = movingBoxes.get(i);
				tmpPos.set(box.body.getPosition());
				batch.setColor(box.color);
				if (box.body != null) {
					renderPos.set(box.body.getPosition().x * BOX_TO_WORLD - Tile.size / 2,
							box.body.getPosition().y * BOX_TO_WORLD - Tile.size / 2);
					batch.draw(box.leftUp, renderPos.x, renderPos.y + 36);
					batch.draw(box.rightUp, renderPos.x + 36, renderPos.y + 36);
					batch.draw(box.leftDown, renderPos.x, renderPos.y);
					batch.draw(box.rightDown, renderPos.x + 36, renderPos.y);
				}
			}
		}
		batch.setColor(1, 1, 1, 1);
	}

	private Vector2 pos = new Vector2();

	public void boxDestinationReached() {
		// if (!world.avatar.keysPressed)
		for (int i = 0; i < movingBoxes.size; i++) {
			if (movingBoxes.get(i).body != null) {
				Box box = movingBoxes.get(i);
				pos.set(box.body.getPosition());
				pos.scl(BOX_TO_WORLD);
				Vector2 dir = box.body.getLinearVelocity();
				if ((dir.x < 0 && pos.x < box.destination.x) || (dir.x > 0 && pos.x > box.destination.x)
						|| (dir.y < 0 && pos.y < box.destination.y) || (dir.y > 0 && pos.y > box.destination.y)) {
					box.stop = true;
					// System.out.println(pos + " " + box.dir + " " +
					// box.destination);
					setDestination(box);
					// box.body.setTransform(box.destination.scl(WORLD_TO_BOX),
					// 0);
					// System.out.println("destination reached");
					world.update = true;
				}
				// if (dir.x != 0)
				// if ((dir.x < 0 && pos.x <= box.destination.x) || (dir.x > 0
				// && pos.x >= box.destination.x)) {
				// xReached = true;
				// dir.x = 0;
				// if (!xKeysPressed)
				// setX(destination.x);
				// if (!yReached)
				// go(false);
				// }
				// if (dir.y != 0)
				// if ((dir.y < 0 && getPos().y <= box.destination.y) || (dir.y
				// > 0 && pos.y >= box.destination.y)) {
				// yReached = true;
				// dir.y = 0;
				// if (!yKeysPressed)
				// setY(destination.y);
				// if (!xReached)
				// go(false);
				// }
			}

		}
	}

	private void resetVisible() {
		// sichtbarflag zurücksetzen
		int i = oldLeft;
		while (true) {
			int j = oldDown;
			while (true) {
				Box box = tiles[i][j].box1;
				if (box != null)
					box.isVisible = false;
				if (j == oldUp)
					break;
				j = (j + 1) % anzTileHeight;
			}
			if (i == oldRight)
				break;
			i = (i + 1) % anzTileWidth;
		}
	}

	private void createBodys(int i, int j) {
		BodyDef tileDef = new BodyDef();
		// tileDef.type = BodyType.DynamicBody;
		tileDef.type = BodyType.StaticBody;
		tileDef.position.set(tilesX[i] * WORLD_TO_BOX, tilesY[j] * WORLD_TO_BOX);
		Body tileBody = world.physics.createBody(tileDef);
		tileBody.setTransform(tilesX[i] * WORLD_TO_BOX, tilesY[j] * WORLD_TO_BOX, 0);
		createFixtures(tileBody);
		tileBody.setUserData(tiles[i][j].box1);
		tiles[i][j].box1.setBody = true;
		tiles[i][j].box1.body = tileBody;
	}

	private void createFixtures(Body body) {
		PolygonShape helpShape = new PolygonShape();
		helpShape.setAsBox(Tile.size / 2 * WORLD_TO_BOX, Tile.size / 2 * WORLD_TO_BOX);

		Vector2 vertex0 = new Vector2();
		Vector2 vertex1 = new Vector2();
		Vector2 vertex2 = new Vector2();
		Vector2 vertex3 = new Vector2();
		helpShape.getVertex(0, vertex0);
		helpShape.getVertex(1, vertex1);
		helpShape.getVertex(2, vertex2);
		helpShape.getVertex(3, vertex3);
		helpShape.dispose();

		FixtureDef ghostVertex1 = new FixtureDef();
		FixtureDef ghostVertex2 = new FixtureDef();
		FixtureDef ghostVertex3 = new FixtureDef();
		FixtureDef ghostVertex4 = new FixtureDef();
		EdgeShape edgeShape1 = new EdgeShape();
		EdgeShape edgeShape2 = new EdgeShape();
		EdgeShape edgeShape3 = new EdgeShape();
		EdgeShape edgeShape4 = new EdgeShape();
		edgeShape1.set(vertex1, vertex0);
		edgeShape2.set(vertex3, vertex2);
		edgeShape3.set(vertex0, vertex3);
		edgeShape4.set(vertex2, vertex1);

		edgeShape1.setVertex3(vertex0.cpy().scl(1.5f).sub(vertex1.cpy().scl(.5f)));
		edgeShape1.setHasVertex3(true);
		edgeShape2.setVertex0(vertex3.cpy().scl(1.5f).sub(vertex2.cpy().scl(.5f)));
		edgeShape2.setHasVertex0(true);

		edgeShape1.setVertex0(vertex1.cpy().scl(1.5f).sub(vertex0.cpy().scl(.5f)));
		edgeShape1.setHasVertex0(true);
		edgeShape2.setVertex3(vertex2.cpy().scl(1.5f).sub(vertex3.cpy().scl(.5f)));
		edgeShape2.setHasVertex3(true);

		edgeShape3.setVertex0(vertex0.cpy().scl(1.5f).sub(vertex3.cpy().scl(.5f)));
		edgeShape3.setHasVertex0(true);
		edgeShape4.setVertex3(vertex1.cpy().scl(1.5f).sub(vertex2.cpy().scl(.5f)));

		edgeShape4.setHasVertex3(true);
		edgeShape3.setVertex3(vertex3.cpy().scl(1.5f).sub(vertex0.cpy().scl(.5f)));
		edgeShape3.setHasVertex3(true);
		edgeShape4.setVertex0(vertex2.cpy().scl(1.5f).sub(vertex1.cpy().scl(.5f)));
		edgeShape4.setHasVertex0(true);

		ghostVertex1.shape = edgeShape1;
		ghostVertex2.shape = edgeShape2;
		ghostVertex3.shape = edgeShape3;
		ghostVertex4.shape = edgeShape4;

		body.createFixture(ghostVertex1);
		body.createFixture(ghostVertex2);
		body.createFixture(ghostVertex3);
		body.createFixture(ghostVertex4);
		edgeShape1.dispose();
		edgeShape2.dispose();
		edgeShape3.dispose();
		edgeShape4.dispose();
	}
}
