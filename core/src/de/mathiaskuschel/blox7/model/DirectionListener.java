package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.math.Vector2;

public interface DirectionListener {
	public void OnDirectionChanged(Vector2 dir);
}
