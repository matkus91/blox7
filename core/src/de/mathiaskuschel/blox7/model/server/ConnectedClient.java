package de.mathiaskuschel.blox7.model.server;

import com.esotericsoftware.kryo.io.Input;

import de.mathiaskuschel.blox7.model.shared.MoveState;

public class ConnectedClient {
	public ConnectedClient() {
		this.MoveState = new MoveState();
	}

	public int ClientId;
	// Most recent input data.
	public Input Input;
	// Number of last received input.
	public int InputNumber;
	/// Client move state on server.
	public MoveState MoveState;

}
