package de.mathiaskuschel.blox7.model;

public interface PlayerListener {


	public void processDirections();

	public void go(boolean backwards);
}
