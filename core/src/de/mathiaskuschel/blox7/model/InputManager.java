package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import de.mathiaskuschel.blox7.model.Avatar.Movement;
//import de.mathiaskuschel.blox7.model.Avatar.Movement;
import de.mathiaskuschel.blox7.view.GameControlsRenderer;

public class InputManager {
	private Array<DirectionListener> directionListeners = new Array<DirectionListener>();
	private Array<DirectionListener> horizontalDirectionListeners = new Array<DirectionListener>();
	private Array<Avatar> avatars = new Array<Avatar>();
	private boolean keyPressed;
	private boolean xKeyPressed;
	private boolean yKeyPressed;

	private boolean oldUpKey = false;

	public boolean isKeyPressed() {
		return keyPressed;
	}

	public boolean isxKeyPressed() {
		return xKeyPressed;
	}

	public boolean isyKeyPressed() {
		return yKeyPressed;
	}

	private boolean oldDownKey = false;
	private boolean oldLeftKey = false;
	private boolean oldRightKey = false;

	private Vector2 keyDir = new Vector2();

	public void addAvatarListener(Avatar avatar) {
		avatars.add(avatar);
	}

	public void directionChanged(Vector2 dir) {
		for (int i = 0; i < avatars.size; i++) {
			avatars.get(i).dir.set(dir);
		}
	}

	public void movementChanged(Movement movement) {
		for (int i = 0; i < avatars.size; i++) {
			avatars.get(i).movement = movement;
		}
	}

	public void processDirections() {
		for (int i = 0; i < avatars.size; i++) {
			avatars.get(i).processDirections();
		}
	}

	public void go(boolean backwards) {
		for (int i = 0; i < avatars.size; i++) {
			avatars.get(i).go(backwards);
		}
	}

	public void processKeys() {
		boolean upKey = Gdx.input.isKeyPressed(Keys.W) || Gdx.input.isKeyPressed(Keys.UP);
		boolean downKey = Gdx.input.isKeyPressed(Keys.S) || Gdx.input.isKeyPressed(Keys.DOWN);
		boolean leftKey = Gdx.input.isKeyPressed(Keys.A) || Gdx.input.isKeyPressed(Keys.LEFT);
		boolean rightKey = Gdx.input.isKeyPressed(Keys.D) || Gdx.input.isKeyPressed(Keys.RIGHT);
		xKeyPressed = leftKey || rightKey;
		yKeyPressed = downKey || upKey;
		keyPressed = upKey || leftKey || downKey || rightKey;
		if (keyPressed) {
			boolean keyAdded = false;
			if (keyDir.y <= 0 && upKey && (!oldUpKey || !downKey)) {
				keyAdded = true;
				keyDir.y = 1;
			} else if (keyDir.y >= 0 && downKey && (!oldDownKey || !upKey)) {
				keyAdded = true;
				keyDir.y = -1;
			}
			if (keyDir.x >= 0 && leftKey && (!oldLeftKey || !rightKey)) {
				keyAdded = true;
				keyDir.x = -1;
			} else if (keyDir.x <= 0 && rightKey && (!oldRightKey || !leftKey)) {
				keyAdded = true;
				keyDir.x = 1;
			}
			if (keyAdded) {
				directionChanged(keyDir);
				movementChanged(Movement.run);
				processDirections();
				go(false);
			}
		} else
			keyDir.set(0, 0);
		oldUpKey = upKey;
		oldDownKey = downKey;
		oldLeftKey = leftKey;
		oldRightKey = rightKey;
	}

	public void processTouchpad() {
		keyPressed = GameControlsRenderer.knopPercentX != 0 || GameControlsRenderer.knopPercentY != 0;
		xKeyPressed = false;
		yKeyPressed = false;
		if (keyPressed) {
			double winkel = Math.atan2(GameControlsRenderer.knopPercentY, GameControlsRenderer.knopPercentX);
			winkel = Math.round(winkel / (Math.PI / 4)) * Math.PI / 4;
			// round ist nicht ideal, aber funktioniert, damit keine Zahlen ganz
			// nah bei null rauskommen
			float helpX = Math.round(Math.cos(winkel));
			float helpY = Math.round(Math.sin(winkel));
			if (helpX != 0) {
				keyDir.x = helpX;
				xKeyPressed = helpX != 0;
			}
			if (helpY != 0) {
				keyDir.y = helpY;
				yKeyPressed = helpY != 0;
			}
			movementChanged(Movement.run);
			processDirections();
			go(false);
		}
	}
}
