package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import de.mathiaskuschel.blox7.model.Avatar.Movement;

public class AvatarRenderer {
	private float stateTime = 0;
	private boolean animDirection;
	private Animation bobLeft;
	private Animation bobRight;
	private Animation bobIdleLeft;
	private Animation bobIdleRight;
	private Animation anim;

	public AvatarRenderer() {
		Texture bobTexture = new Texture(Gdx.files.internal("img/bob.png"));
		TextureRegion[] split = new TextureRegion(bobTexture).split(20, 20)[0];
		TextureRegion[] mirror = new TextureRegion(bobTexture).split(20, 20)[0];
		for (TextureRegion region : mirror)
			region.flip(true, false);
		bobRight = new Animation(0.1f, split[0], split[1]);
		bobLeft = new Animation(0.1f, mirror[0], mirror[1]);
		bobIdleRight = new Animation(0.5f, split[0], split[4]);
		bobIdleLeft = new Animation(0.5f, mirror[0], mirror[4]);
		anim = bobIdleRight;
	}

	public void renderAvatar(SpriteBatch batch, float delta, Avatar avatar) {
		stateTime += delta;
		setAnim(avatar.movement);
		// batch.setProjectionMatrix(camera.combined);
		batch.draw(anim.getKeyFrame(stateTime, true), avatar.getPos().x - Tile.size / 2,
				avatar.getPos().y - Tile.size / 2, Tile.size, Tile.size);
	}

	public void setAnim(Movement movement) {
		switch (movement) {
		case run:
			if (animDirection)
				anim = bobLeft;
			else
				anim = bobRight;
			break;
		case idle:
			if (animDirection)
				anim = bobIdleLeft;
			else
				anim = bobIdleRight;
			break;
		default:
			break;
		}
	}
}
