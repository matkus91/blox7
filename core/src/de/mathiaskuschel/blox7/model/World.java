package de.mathiaskuschel.blox7.model;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Array;

import de.mathiaskuschel.blox7.model.Avatar.Movement;

public class World {
	public Avatar avatar;
	public com.badlogic.gdx.physics.box2d.World physics;
	public static final float WORLD_TO_BOX = 1f / 36f;
	public static final float BOX_TO_WORLD = 36;
	public Array<Box> collidedBoxes = new Array<Box>();
	public boolean moveBox;
	// public HashSet<Body> collidedBoxes = new HashSet<Body>();
	// public Body collidedBox;

	public PlayingField playingField;
	public boolean update;
	public GameClient gameClient;
	public GameServer gameServer;

	public World() {
		if (Gdx.app.getType() == ApplicationType.Desktop)
			gameServer = new GameServer();
		else
			gameClient = new GameClient();
		playingField = new PlayingField(this, 100, 70);
		physics = new com.badlogic.gdx.physics.box2d.World(new Vector2(0, 0), true);
		physics.setContactListener(new ContactListener() {
			@Override
			public void beginContact(Contact contact) {
				avatar.collided++;
				// System.out.println(contact.getFixtureA().getBody().getPosition());
				Body block1 = contact.getFixtureA().getBody();
				Body block2 = contact.getFixtureB().getBody();
				Box box1 = (Box) block1.getUserData();
				Box box2 = (Box) block2.getUserData();
				Box collidedBox = null;
				if (block1 == avatar.playerBody)
					collidedBox = box2;
				else if (block2 == avatar.playerBody)
					collidedBox = box1;

				// wenn Avatar mit collidedBox kollidiert
				if (collidedBox != null) {
					collidedBox.contact = contact;
					collidedBox.push = true; // box wird berührt
					if (!collidedBoxes.contains(collidedBox, true)) {
						Vector2 pos1 = collidedBox.body.getPosition();
						Vector2 pos2 = avatar.playerBody.getPosition();
						float div = Math.abs(pos2.x - pos1.x) - Math.abs(pos2.y - pos1.y);
						if (div != 0) {
							if (div > 0)
								collidedBox.dir.set(pos1.x - pos2.x, 0).nor();
							else
								collidedBox.dir.set(0, pos1.y - pos2.y).nor();
							// wenn Avatar nicht schräg läuft, Kolissionnormale
							// ist
							// leider nie schräg
							Vector2 normal = contact.getWorldManifold().getNormal();
							System.out.println("normale: " + normal);
							collidedBoxes.add(collidedBox);
							collidedBox.vertical = normal.x == 0;
							// normale oft falschrum, deswegen nur Betrag
							// speichern
							// und mit Avatar Richtung skalieren
							abs(normal);
							// collidedBox.dir.set(normal.scl(playerBody.getLinearVelocity())).nor();
							System.out.println("playervelo: " + avatar.playerBody.getLinearVelocity());
							System.out.println("Boxdir: " + collidedBox.dir);
							collidedBox.hadContact = true;
						}
					} else {
						// Wenn Box schon in der Liste ist, muss überprüft
						// werden ob es erst in diesem Tick hinzugefügt wurde,
						// wenn nicht, dann ist es wiederholte Kollision
						System.out.println("body already in list");
						collidedBox.stop = false;
						if (!playingField.movingBoxes.contains(collidedBox, true))
							avatar.repeatedCollision = true;
					}

				} // else {
					// wenn zwei Blöcke miteinander kollidieren
					// Box direction setzen
					// if (block1.getType() == BodyType.DynamicBody) {
					// Vector2 help = contact.getWorldManifold().getNormal();
					// abs(help);
					// box1.dir.set(help.scl(block2.getLinearVelocity()));
					// box1.stop = true;
					// }
					// if (block2.getType() == BodyType.DynamicBody) {
					// Vector2 help = contact.getWorldManifold().getNormal();
					// abs(help);
					// box2.dir.set(help.scl(block1.getLinearVelocity()));
					// box2.stop = true;
					// }
					// }
					// neu rendern
				update = true;
			}

			private void abs(Vector2 vec) {
				vec.x = Math.abs(vec.x);
				vec.y = Math.abs(vec.y);
			}

			@Override
			public void endContact(Contact contact) {
				Body block1 = contact.getFixtureA().getBody();
				Body block2 = contact.getFixtureB().getBody();
				Box box1 = (Box) block1.getUserData();
				Box box2 = (Box) block2.getUserData();
				System.out.println("endcontact");
				if (block2 == avatar.playerBody) {
					// System.out.println("block1");
					if (collidedBoxes.contains(box1, true))
						collidedBoxes.removeValue(box1, true);
					box1.push = false;
					if (playingField.pushedBox == box1)
						playingField.pushedBox = null;
					// System.out.println("box1 contact end");
				}

				if (block1 == avatar.playerBody) {
					// System.out.println("block2");
					if (collidedBoxes.contains(box2, true))
						collidedBoxes.removeValue(box2, true);
					box2.push = false;
					if (playingField.pushedBox == box2)
						playingField.pushedBox = null;
					// System.out.println("box2 contact end");
				}
				// if (block1 != playerBody && block2 != playerBody) {
				// if (stoppedBoxes.contains(block1, true))
				// stoppedBoxes.removeValue(block1, true);
				// if (stoppedBoxes.contains(block2, true))
				// stoppedBoxes.removeValue(block2, true);
				// }

				// neu rendern
				update = true;

				// stoppedBoxes.add(block2);
				// System.out.println("zwei Blöcke sind miteinander
				// kollidiert");
				// }
			}

			@Override
			public void preSolve(Contact contact, Manifold oldManifold) {
				// System.out.println("lokale normale: " +
				// oldManifold.getLocalNormal());
				// System.out.println("normale: " +
				// contact.getWorldManifold().getNormal());
				// Body block1 = contact.getFixtureA().getBody();
				// Body block2 = contact.getFixtureB().getBody();
				// Box box1 = (Box) block1.getUserData();
				// Box box2 = (Box) block2.getUserData();
				// Box collidedBox = null;
				// if (block1 == playerBody) {
				// collidedBox = box2;
				// } else if (block2 == playerBody)
				// collidedBox = box1;
				// if (collidedBox != null) {
				// Vector2 normal = oldManifold.getLocalNormal();
				// collidedBox.diagonal = normal.x != 0 && normal.y != 0;
				// System.out.println("lokale normale: " + normal);
				// System.out.println("diagonal: " + collidedBox.diagonal);
				// }
			}

			@Override
			public void postSolve(Contact contact, ContactImpulse impulse) {
			}

		});
		avatar = new Avatar(this);
		inputManager.addAvatarListener(avatar);
	}

	private Vector2 veloInBox = new Vector2();
	private Vector2 helpVelocityVector = new Vector2();
	public InputManager inputManager = new InputManager();

	public void update(float delta) {
		// Auf dem Desktop: Tastatureingaben
		if (Gdx.app.getType() != ApplicationType.Android && Gdx.app.getType() != ApplicationType.iOS)
			inputManager.processKeys();
		// Ansonsten: Touchpad
		else
			inputManager.processTouchpad();
		// if (movement == Movement.run)
		// pos.add(delta * velocityVecor.x, delta * velocityVecor.y);

		// Geschwindigkeit ausrechnen -> box2d Position bestimmen -> Avatar
		// position danach setzen
		if (avatar.movement == Movement.run) {
			helpVelocityVector.set(avatar.getVelocityVector());
			veloInBox.set(helpVelocityVector.scl(WORLD_TO_BOX));
			avatar.playerBody.setLinearVelocity(veloInBox);
		}
		// System.out.println("tick");
		physics.step(delta, 6, 2);
		// System.out.println("tick end");
		if (avatar.movement == Movement.run) {
			avatar.destinationReached();
			if (!inputManager.isKeyPressed() && (avatar.xReached && avatar.yReached))
				avatar.movement = Movement.idle;
			avatar.checkCollided();
		}
		playingField.processColissions();
	}

	static enum Direction {
		left, right, up, down
	}
}
