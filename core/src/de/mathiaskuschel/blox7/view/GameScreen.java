package de.mathiaskuschel.blox7.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool.PooledEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Array;

import de.mathiaskuschel.blox7.model.AvatarRenderer;
import de.mathiaskuschel.blox7.model.Tile;
import de.mathiaskuschel.blox7.model.World;

public class GameScreen implements Screen {
	public static final float WORLD_TO_BOX = 1f / 36f;
	public static final float BOX_TO_WORLD = 36;
	private OrthographicCamera camera;
	private OrthographicCamera staticCamera;
	private OrthographicCamera extendedCamera;
	// private Box2DDebugRenderer debugRenderer = new Box2DDebugRenderer();
	Array<PooledEffect> effects = new Array<ParticleEffectPool.PooledEffect>();
	private Vector3 oldCamPos;
	private World world;
	private AvatarRenderer avatarRenderer = new AvatarRenderer();
	// private FPSLogger fps = new FPSLogger();

	@Override
	public void show() {
		font.getData().setScale(2f, 2f);
		// (int) Math.ceil(viewport.getWorldWidth() / Tile.size),
		// (int) Math.ceil(viewport.getWorldHeight()) / Tile.size);
		world = new World();
		camera = new OrthographicCamera();
		staticCamera = new OrthographicCamera();
		extendedCamera = new OrthographicCamera();
		oldCamPos = new Vector3();
		debugRenderer = new Box2DDebugRenderer();

		// PolygonShape playerShape = new PolygonShape();
		// playerShape.setAsBox(avatar.bounds.width / 2 * WORLD_TO_BOX,
		// avatar.bounds.height / 2 * WORLD_TO_BOX);
		//
		// FixtureDef fixtureDef = new FixtureDef();
		// fixtureDef.shape = playerShape;

		ParticleEffectPool bombEffectPool;
		// TextureAtlas atlas = new TextureAtlas();
		ParticleEffect bombEffect = new ParticleEffect();
		bombEffect.load(Gdx.files.internal("particles/bomb.p"), Gdx.files.internal("img"));
		bombEffect.setEmittersCleanUpBlendFunction(true);
		bombEffectPool = new ParticleEffectPool(bombEffect, 1, 2);

		// Create effect:
		PooledEffect effect = bombEffectPool.obtain();
		effect.setPosition(world.avatar.getPos().x, world.avatar.getPos().y);
		effects.add(effect);

		debugRenderer.setDrawAABBs(true);
		debugRenderer.setDrawBodies(true);
		debugRenderer.setDrawContacts(true);
		debugRenderer.setDrawInactiveBodies(false);
		debugRenderer.setDrawJoints(true);
		debugRenderer.setDrawVelocities(true);
	}

	private SpriteBatch batch = new SpriteBatch();
	private BitmapFont font = new BitmapFont();
	private GameControlsRenderer controlRenderer = new GameControlsRenderer(batch);
	private boolean effects_fboEnabled = true;
	private FrameBuffer effects_fbo = null;
	private TextureRegion m_fboRegion = null;
	private FrameBuffer bg_fbo = null;
	private TextureRegion bg_fboRegion = null;
	private Box2DDebugRenderer debugRenderer;

	// Pufferzone, die Außerhalb des screens gerendert wird
	private int deltaX = Tile.size * 2;
	private int deltaY = Tile.size * 2;

	@Override
	public void render(float delta) {
		// boolean render =
		world.update(delta);
		camera.position.set(world.avatar.getPos(), 0);
		camera.update();
		extendedCamera.position.set(world.avatar.getPos(), 0);
		extendedCamera.update();
		batch.setProjectionMatrix(camera.combined);
		batch.begin();
		if (effects_fboEnabled) // enable or disable the supersampling
			effects_fbo.begin();
		// Gdx.gl.glBlendFuncSeparate(GL20.GL_SRC_ALPHA,
		// GL20.GL_ONE_MINUS_SRC_ALPHA, GL20.GL_ONE,
		// GL20.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl.glClearColor(0, 0, 0, 0);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		for (int i = effects.size - 1; i >= 0; i--) {
			PooledEffect effect = effects.get(i);
			effect.setPosition(world.avatar.getPos().x, world.avatar.getPos().y);
			effect.draw(batch, delta);
			if (effect.isComplete()) {
				// effect.free();
				// effects.removeIndex(i);
				effect.reset();
			}
		}
		batch.end();
		effects_fbo.end();

		// long start = System.nanoTime();

		// Spielfeld zwischenspeichern
		if (world.playingField.update(extendedCamera) || Math.abs(oldCamPos.x - extendedCamera.position.x) >= deltaX
				|| Math.abs(oldCamPos.y - extendedCamera.position.y) >= deltaY || world.update) {
			world.update = false;

			batch.setProjectionMatrix(extendedCamera.combined);
			bg_fbo.begin();
			batch.begin();
			Gdx.gl.glClearColor(0, 0, 1, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			world.playingField.render(extendedCamera, batch);
			batch.end();
			bg_fbo.end();
			// System.out.println(System.nanoTime() - start);
			oldCamPos.set(camera.position);
		}

		// Spielfeld zeichnen
		batch.setProjectionMatrix(staticCamera.combined);
		batch.begin();
		batch.draw(bg_fboRegion, oldCamPos.x - camera.position.x - deltaX, oldCamPos.y - camera.position.y - deltaY);

		// Avatar zeichnen
		batch.setProjectionMatrix(camera.combined);
		world.playingField.renderDynamics(batch);
		avatarRenderer.renderAvatar(batch, delta, world.avatar);
		// batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE);
		batch.setProjectionMatrix(staticCamera.combined);
		// start = System.nanoTime();
		batch.draw(m_fboRegion, 0, 0);
		batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);

		batch.end();
		// System.out.println(System.nanoTime() - start);

		Matrix4 cameraCopy = camera.combined.cpy().scl(World.BOX_TO_WORLD);
		debugRenderer.render(world.physics, cameraCopy);

		// Touchpad zeichnen
		controlRenderer.render();
		// batch.begin();
		// playingField.render(viewport.getCamera(), batch);
		// batch.setColor(1, 1, 1, 1);

		// Matrix4 cameraCopy = viewport.getCamera().combined.cpy().scl(
		// BOX_TO_WORLD);
		// debugRenderer.render(world, cameraCopy);

		// batch.end();
		// fps.log();
	}

	@Override
	public void resize(int width, int height) {
		// viewport.update(width, height, true);
		// viewport.getCamera().

		float worldHeight = (float) Math.sqrt(1280 * 720 * height / (double) width);
		camera.setToOrtho(false, 1280 * 720f / worldHeight, worldHeight);
		oldCamPos.set(camera.position);
		// staticCamera.zoom = 2;
		staticCamera.setToOrtho(false, camera.viewportWidth, camera.viewportHeight);
		extendedCamera.setToOrtho(false, camera.viewportWidth + deltaX * 2, camera.viewportHeight + deltaY * 2);
		controlRenderer.resize(width, height);

		effects_fbo = new FrameBuffer(Format.RGBA8888, (int) camera.viewportWidth, (int) camera.viewportHeight, false);
		m_fboRegion = new TextureRegion(effects_fbo.getColorBufferTexture());
		m_fboRegion.flip(false, true);
		bg_fbo = new FrameBuffer(Format.RGBA8888, (int) camera.viewportWidth + deltaX * 2,
				(int) camera.viewportHeight + deltaY * 2, false);
		bg_fboRegion = new TextureRegion(bg_fbo.getColorBufferTexture());
		bg_fboRegion.flip(false, true);

		// batch.setProjectionMatrix(camera.combined);
		// batch.begin();
		// bg_fbo.begin();
		// Gdx.gl.glClearColor(0, 0, 0, 0);
		// Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// playingField.render(batch);
		// bg_fbo.end();
		// batch.end();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}

}
