package de.mathiaskuschel.blox7.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class GameControlsRenderer {
	private Stage stage;
	private Touchpad touchpad;
	private TouchpadStyle touchpadStyle;
	private Skin touchpadSkin;
	private Drawable touchBackground;
	private Drawable touchKnob;
	public static float knopPercentX;
	public static float knopPercentY;

	public GameControlsRenderer(SpriteBatch batch) {
		touchpadSkin = new Skin();
		touchpadSkin.add("touchBackground", new Texture(
				"data/touchBackground.png"));
		touchpadSkin.add("touchKnob", new Texture("data/touchKnob.png"));
		touchpadStyle = new TouchpadStyle();
		touchBackground = touchpadSkin.getDrawable("touchBackground");
		touchKnob = touchpadSkin.getDrawable("touchKnob");
		touchpadStyle.background = touchBackground;
		touchpadStyle.knob = touchKnob;
		touchpad = new Touchpad(30, touchpadStyle);
		stage = new Stage(new FitViewport(1280, 720), batch);
		stage.addActor(touchpad);
		Gdx.input.setInputProcessor(stage);
	}

	public void render() {
		stage.getViewport().apply(true);
		knopPercentX = touchpad.getKnobPercentX();
		knopPercentY = touchpad.getKnobPercentY();
		stage.draw();
	}

	public void resize(int width, int height) {

		float worldHeight = (float) Math.sqrt(1280 * 720 * height
				/ (double) width);
		stage.getViewport()
				.setWorldSize(1280 * 720f / worldHeight, worldHeight);
		stage.getViewport().apply();
		stage.getViewport().update(width, height, true);
		float sizeTouchpad = 300;
		touchpad.setBounds(15, 15, sizeTouchpad, sizeTouchpad);
		touchpad.getStyle().knob.setMinWidth(sizeTouchpad / 3f);
		touchpad.getStyle().knob.setMinHeight(sizeTouchpad / 3f);
	}
}
