package de.mathiaskuschel.blox7;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

import de.mathiaskuschel.blox7.view.GameScreen;

public class Blox7 extends Game {
	@Override
	public void create() {
		setScreen(new GameScreen());
	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}
}
